# Frontend 

- Определение маршрутов пользователя
- Внесение точек перемещения на маршруте
- Контроль местоположения BT, GPS, CELL
- Расписание пользователя на маршруте

## <b>Резпозитории</b>

1) ### <a href="https://gitlab.com/hackKinbery/hackkinbery">Backend</a> - node js сервер 
2) ### <a href="https://gitlab.com/hackKinbery/frontend">Frontend</a> - веб сервис для друзей
3) ### <a href="https://gitlab.com/hackKinbery/mobile">Mobile</a> - мобильный микросервис для поиска ребёнка
4) ### <a href="https://gitlab.com/hackKinbery/kinberyjoint">Описание проекта</a>  
5) ### <a href="https://gitlab.com/hackKinbery/model">Моделирование положения Python</a>  
