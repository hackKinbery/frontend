
let address = "http://127.0.0.1:8000";


/*$.get({
    url: address + "/api/monitor/getParentMonitors/2",
    dataType: 'json',
    success: function(data){
        console.log(data);
    },
    error: function (response) {
        console.log(response)
    }
});*/
$(document).ready(() => {
    $("#userId1").on('click', (() =>{
        initMap(1,2);
        hideContentAndShowUser();
    }));

    $("#userId2").on('click', (() =>{
        initMap(1,15);
    }));

    $("#userId3").on('click', (() =>{
        initMap(1,4);
    }));

    $("#schedulerCreate").on('click', (() =>{
        hideUserAndShowScheduler();
    }));

    $("#backToMain").on('click', (() =>{
        hideContentAndShowUser();
    }));


    initMap();
});

function hideContentAndShowUser(){
    $("#allUser").css('display', 'none');
    $("#userContent").css('display', 'block');
    $("#scheduler").css('display', 'none');
    initMap();
}

function hideUserAndShowScheduler(){
    $("#userContent").css('display', 'none');
    $("#scheduler").css('display', 'block');
}

function initMap(parentId, childId) {
    parentId = parentId || 1;
    childId = childId || 2;

    $.get({ //104.248.102.232:8000
        url: "http://104.248.102.232:8000/api/location/getChildTrackByParentId/"+parentId+"/"+childId,
        dataType: 'json',
        success: function(data){
            let firstLat = null;
            let map = null;
            let prev = null;
            let directions = [];
            let directionsService = new google.maps.DirectionsService();
            let directionsRenderer = new google.maps.DirectionsRenderer();
            let isStart, isEnd;
            data.forEach((c, i) => {
                let toSplit = c.Get_Track_byGPS;
                if(toSplit) {
                    let latlong = {lat: Number(toSplit.split(",")[0].replace("(", "")), lng: Number(toSplit.split(",")[1])};
                    if (!firstLat) {
                        firstLat  = latlong;
                        if(!map) {
                            isStart = true;
                            var customMapType = new google.maps.StyledMapType([
                                {
                                    elementType: 'labels',
                                    stylers: [{visibility: 'off'}]
                                }
                            ], {
                                name: 'Custom Style'
                            });
                            var customMapTypeId = 'custom_style';
                            map = new google.maps.Map(document.getElementById("map"), {
                                zoom: 15,
                                center: firstLat,
                                mapTypeControlOptions: {
                                    mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
                                }
                            });
                            map.mapTypes.set(customMapTypeId, customMapType);
                            map.setMapTypeId(customMapTypeId);
                        }
                    }
                    if(map) {
                        if(i == data.length-1) isEnd = true;
                        let marker = new google.maps.Marker({
                            position: latlong,
                            map,
                            icon: (isStart ? './src/assets/images/start.png' : (isEnd ? "./src/assets/images/end.png"  : "./src/assets/images/other.png")),
                            title: "Battery: (" + toSplit.split(",")[3] + "%",
                        });
                        if(isStart===true) isStart = false;
                        directions.push(latlong);
                        if(prev && latlong) {

                            //var start = new google.maps.LatLng(prev.lat, prev.lng);
                            //var end = new google.maps.LatLng(latlong.lat, latlong.lng);
                            //var request = {
                            //    origin: start,
                            //    destination: end,
                            //    travelMode: google.maps.TravelMode.DRIVING
                            //};
                            //directionsService.route(request, (response, status)=> {
                            //    directionsRenderer.setDirections(response);
                            //    directionsRenderer.setMap(map);
                            //});

                        }
                    }
                    prev = JSON.parse(JSON.stringify(latlong));
                }

            });
            const flightPath = new google.maps.Polyline({
                path: directions,
                geodesic: true,
                strokeColor: "#FF0000",
                strokeOpacity: 1.0,
                strokeWeight: 2,
            });

            flightPath.setMap(map);


            document.getElementById("map").childNodes[0].style.overflow = 'hidden'
        },
        error: function (response) {
            console.log(response);
            return null;
        }
    });
//


}

let monitor = {"parentId": 2, "childId": 15};
/*

$.post({
    url: address + "/api/monitor/saveMonitoring",
    dataType: 'json',
    contentType : 'application/json',
    data: JSON.stringify(monitor),
    success: function(data){
        console.log(data);
    },
    error: function (response) {
        console.log(response)
    }
});*/
